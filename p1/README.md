> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Mobile Web Application Development

## Ariana M. Davis

### Project 1 Requirements:

1. Create and Display Launcher Icon In App
2. Create and Display a Splash Screen
3. Use OnClickListener's to Play/Pause Music

#### README.md file should include the following items:

* Screenshot of Splash Page 
* Screenshot of Non - Playing Music

#### Assignment Screenshots: 

|*Splash Page:*|Non - Playing Music Page:|
|-----------------|------------------|------------------|
|![Splash Page Screenshot](img/splash.png)|![Non - Playing Music Screenshot](img/music.png)