> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Mobile Web Application Development

## Ariana M. Davis

### Assignment 5 Requirements:

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1
* Screenshot of running application’s splash screen(list of articles –activity_items.xml
* Screenshotofrunning application’s individual article(activity_item.xml)
* Screenshotsofrunning application’sdefault browser(article link)

#### Assignment Screenshots: 

|*Items Page:*|*Item Page*|Read More:|
|-----------------|------------------|------------------|
|![Items Page Screenshot](img/items.png)|![Item Page Screenshot](img/item.png)|![Read More Screenshot](img/read.png)|