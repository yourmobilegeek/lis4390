> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Mobile Web Application Development

## Ariana M. Davis

### Assignment 3 Requirements:

#### README.md file should include the following items:

* Screenshot of app with landing screen 
* Screenshot of app with error mesage
* Screenshot of app with currency result

#### Assignment Screenshots: 

|*Landing Page:*|*Error Notification:*|Result Page:|
|-----------------|------------------|------------------|
|![Landing PageScreenshot](img/launch.png)|![Error Notification screenshot](img/error.png)|![Converted Currency screenshot](img/result.png)|