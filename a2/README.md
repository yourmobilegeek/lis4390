> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Mobile Web Application Development

## Ariana M. Davis

### Assignment 2 Requirements:

#### README.md file should include the following items:

* Screenshot of app before it populates
* Screenshot of app after it populates

#### Assignment Screenshots: 

*Screenshot Before*:
![Before Screenshot](img/before.png)

*Screenshot After*:
![After Screenshot](img/after.png)