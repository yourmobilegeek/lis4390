> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Mobile Web Application Development

## Ariana M. Davis

### Assignment 4 Requirements:

#### README.md file should include the following items:

* Screenshot of app with running application splash screen 
* Screenshot of app with invalid screen
* Screenshot of app with valid result

#### Assignment Screenshots: 

|*Splash Page:*|*InValid Page*|Valid Page:|
|-----------------|------------------|------------------|
|![Splash Screen Screenshot](img/splash.png)|![InValid Screenshot](img/error.png)|![Valid Screenshot](img/valid.png)|