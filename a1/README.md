> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Mobile Web Application Development

## Ariana M. Davis

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)
4. Bitbucket repo links at this assignment and by the completed tutorial above bitbucketstations content

#### README.md file should include the following items:

* Screenshot of Contact Apps;
* Screenshot of running java Hello;
* Screenshot of running Android Studio - My First App
* git commands w/short descriptions;
* Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes).

> #### Git commands w/short descriptions:

1. git init - creates an empty Git repository - basically a git directory
2. git status - displays paths that have differences between the index file and the current HEAD commit
3. git add - updates the index using the current content found in the working tree 
4. git commit - records changes to the repository
5. git push - update remote refs along with associated objects 
6. git pull - fetch from and intergrate with another repository or a local branch
7. git merge - join two or more development histories together

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/java_hello.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android_hello.png)

*Screenshot of Contact Apps*:

![Contacts App Screenshot](img/contact_card1.png)

![Contacts App 2 Screenshot](img/contact_card2.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/yourmobilegeek/bitbucketstationlocations "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/yourmobilegeek/myteamquotes "My Team Quotes Tutorial")