> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Mobile Web Application Development

## Ariana M. Davis

### Project 2 Requirements:

1. Insert at least five sample tasks
2. Test database class 
3. Must add background color(s) or theme
4. Create and display launcher icon image

#### README.md file should include the following items:

* Screenshot of Task List Activty

#### Assignment Screenshots: 

|*Task List Activity:*|
|-----------------|------------------|------------------|
|![Task List Acitivyt Screenshot](img/tasklist.png)|