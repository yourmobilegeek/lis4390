> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Ariana M. Davis 

### LIS4930 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Android Studio and create My First App and Contacts App
    - Provide screenshots and installations  
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - This assignment has us create an app that helps splits the bill at the end of a meal 
    - Formatted out currency 
    - Implemeneted Drop Down Spinners 
    - Returned final result to a TextView 

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - This assignment had us convert currency from U.S. Dollars to the following ( euros, pesos, canadian dollars )
    - Used Radio Buttons and Radio Groups
    - Returned final result to a TextView
    
4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create and Display Launcher Icon
    - Create and Display Splash Screen
    - Play and Pause Music Using OnClickListener

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Include splash screen image,app title, intro text
    - Inlcude images
    - Create and display launcher icon image
    - Calculate Interest

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Include splash screen with app title and list of articles
    - Must find and use your ownRSS feed 
    - Must add background color(s) or theme
    - Create and displaylauncher icon image

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Insert at least five sample tasks
    - Test database class 
    - Must add background color(s) or theme
    - Create and display launcher icon image



